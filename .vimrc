" Remapping keys
" Set the user leader key to space, default '\'
let mapleader="\<Space>" 

nmap <leader>ne :NERDTreeToggle<cr>
" Remap vim's search to use 'normal' regex
nnoremap / /\v
vnoremap / /\v
" Map . in visual mode to repeat action over all selected lines
vnoremap . :norm.<CR>

" Change split navigation
noremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Try these lines to speed up the annoying lag witnessed by vim
set nocursorline    " don't paint cursor line
set nocursorcolumn  " don't paint cursor column
set nolazyredraw      " don't wait to redraw
set noshowcmd
set termguicolors " enable true colour support
set number

syntax on " Enable syntax highlighting
set hidden " Unsaved buffers will be hidden rather than closed
set spell spelllang=en,es " Turn on spell checking and set to english and españa

" Searching
set hlsearch " Search as characters are entered
set incsearch " Highlight matches

execute pathogen#infect()
" Required:
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
"

" Autocmd 
augroup markdown
    autocmd!
    autocmd BufRead,BufNewFile *.txt,*.TXT set filetype=markdown " Set markdown syntax to be used with txt files
augroup END

" Plugin settings

" Colors! Moved to the bottom so that the colorscheme works
" with the plugin version
" Other themes : kolor, light, sol, murmur, aurora, base16, molokai, luna,
" lucius, cobalt2 
let g:airline_theme='kolor'
colorscheme delek_extended
let g:palenight_terminal_italics=1
set guifont=Menlo-Regular:h16

" Allows syntax highlighting in fenced code blocks, add more languages as you feel the need
let g:markdown_fenced_languages=['html', 'javascript', 'css', 'json=javascript', 'ruby', 'python', 'vim', 'sass', 'sh', 'zsh=sh', 'bash=sh'] 
