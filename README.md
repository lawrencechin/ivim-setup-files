# iVim Setup Files

Basic set up for [iVim](https://github.com/terrychou/iVim) including `.vimrc` and a color scheme. Plugins need to be downloaded separately. I would recommend using [Working Copy](https://workingcopyapp.com) to download repositories and then move them to the `bundle` folder.

I guess if one were inclined, one could add submodules to the `bundle` folder and then simply grab this repository and move it to the *iVim* folder. Just a thought.

## Currently Installed Plugins

* 'tpope/vim-pathogen'
* 'vim-airline/vim-airline' " Status bar
* 'benekastah/neomake' " Linting plugin
* 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' } " Sidebar
* 'vim-airline/vim-airline-themes' " Themes for airline
* 'othree/html5.vim' " HTML5 syntax
* 'pangloss/vim-javascript' " js syntax
* 'hail2u/vim-css3-syntax' " CSS3 highlights
* 'mxw/vim-jsx' " JSX syntax highlighting
* 'drewtempelmeyer/palenight.vim' " Dark theme
