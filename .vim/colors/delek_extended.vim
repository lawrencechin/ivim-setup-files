" Vim color file
" Maintainer:   Lawrence Chin	
" Last Change:	2019 Feb 16
"
" Note - The main purpose of this theme is to work as a 
" pass through for the colours set in the terminal.
" This allows for an easy dark or light theme switch.
"
" The gui version doesn't work this way so I've hardcoded 
" a version of Fresh Air and no dark theme
"
" Colours
" Background #feffff
" Foreground #000000
" Bold Text #688088
" Selection #4e279a
" Black #000000
" Red #eb4b3f
" Green #00990f
" Yellow #dc6122
" Blue #603da8
" Magenta #e02da8
" Cyan #009cd9
" White #a2b0b0
" Bright Black #688088
" Bright Red #eb4b3f
" Bright Green #00990f
" Bright Yellow #dc6122
" Bright Blue #7c86ce
" Bright Magenta #ff6ca1 
" Bright Cyan #009cd9
" Bright White #a2b0b0
" Cursor #688088
"

highlight clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "delek_extended"

" Normal should come first
hi Normal     guifg=#000000  guibg=#feffff
hi Cursor     guifg=bg     guibg=#4e279a
hi lCursor    guifg=NONE   guibg=#009cd9

" Note: we never set 'term' because the defaults for B&W terminals are OK
hi DiffAdd    ctermbg=LightBlue    guibg=#7c86ce
hi DiffChange ctermbg=LightMagenta guibg=#ff6ca1
hi DiffDelete ctermfg=Blue	   ctermbg=LightCyan gui=bold guifg=#603da8 guibg=#009cd9
hi DiffText   ctermbg=Red	   cterm=bold gui=bold guibg=#eb4b3f
hi Directory  ctermfg=DarkBlue	   guifg=#603da8
hi ErrorMsg   cterm=bold,underline gui=bold,underline
hi Error      cterm=bold,underline ctermfg=DarkRed ctermbg=NONE gui=bold,underline guifg=#eb4b3f guibg=NONE
hi FoldColumn ctermfg=DarkBlue	   ctermbg=NONE     guibg=NONE	    guifg=#eb4b3f
hi Folded     ctermbg=None	   ctermfg=DarkBlue guibg=NONE guifg=#eb4b3f
hi IncSearch  cterm=reverse	   gui=reverse
hi LineNr     ctermfg=Brown	   guifg=Brown
hi ModeMsg    cterm=bold	   gui=bold
hi MoreMsg    ctermfg=DarkGreen    gui=bold guifg=#00990f
hi NonText    ctermfg=Blue	   gui=bold guifg=#603da8 
hi Pmenu      guibg=#ff6ca1
hi PmenuSel   ctermfg=White	   ctermbg=DarkBlue  guifg=White  guibg=#603da8
hi Question   ctermfg=DarkGreen    gui=bold guifg=#00990f
hi Search     cterm=underline,bold ctermfg=DarkGreen ctermbg=NONE guifg=#00990f guibg=NONE gui=underline,bold
hi SpecialKey ctermfg=DarkBlue	   guifg=#603da8
hi clear StatusLine " something overrides this setting requiring it to be cleared first
hi StatusLine cterm=none ctermbg=DarkBlue ctermfg=White gui=none guibg=#603da8 guifg=White
hi StatusLineNC	cterm=none ctermbg=LightBlue ctermfg=White  guibg=#7c86ce gui=none guifg=White
hi Title      ctermfg=DarkMagenta  gui=bold guifg=#e02da8
hi clear VertSplit
hi Visual     ctermbg=NONE	   cterm=reverse gui=reverse guibg=NONE
hi WarningMsg ctermfg=DarkRed	   guifg=#eb4b3f
hi WildMenu   ctermfg=Black	   ctermbg=Yellow    guibg=#dc6122 guifg=Black
hi SpellBad cterm=underline ctermbg=NONE ctermfg=DarkRed gui=underline guibg=NONE guifg=#eb4b3f
hi SpellRare cterm=underline ctermbg=NONE ctermfg=DarkGreen gui=underline guibg=NONE guifg=#00990f
hi SpellCap cterm=underline ctermbg=NONE ctermfg=DarkYellow gui=underline guibg=NONE guifg=#cb416
hi SpellLocal cterm=underline ctermbg=NONE ctermfg=Brown gui=underline guibg=NONE guifg=Brown
hi clear SignColumn 
hi MatchParen cterm=underline ctermbg=NONE gui=underline guibg=NONE

" Tabs, should one ever use them
hi TabLine      cterm=underline ctermbg=none ctermfg=grey gui=underline guibg=none guifg=#688088
hi TabLineFill  cterm=underline ctermbg=none gui=underline guibg=none
hi TabLineSel   cterm=bold gui=bold

" syntax highlighting
hi Comment    cterm=NONE ctermfg=DarkRed     gui=NONE guifg=#eb4b3f
hi Constant   cterm=NONE ctermfg=DarkGreen   gui=NONE guifg=#00990f
hi Identifier cterm=NONE ctermfg=DarkCyan    gui=NONE guifg=#009cd9
hi PreProc    cterm=NONE ctermfg=DarkMagenta gui=NONE guifg=#e02da8
hi Special    cterm=NONE ctermfg=Red    gui=NONE guifg=#eb4b3f
hi Statement  cterm=bold ctermfg=Blue	     gui=bold guifg=#603da8
hi Type	      cterm=NONE ctermfg=Blue	     gui=NONE guifg=#603da8

" Markdown Highlighting
hi markdownHeadingDelimiter ctermfg=DarkRed guifg=#eb4b3f
hi markdownRule ctermfg=DarkRed guifg=#eb4b3f
hi markdownItalic ctermfg=DarkCyan cterm=italic guifg=#009cd9 gui=italic
hi markdownBold ctermfg=DarkGreen cterm=bold guifg=#00990f gui=bold
hi markdownBoldItalic ctermfg=DarkGreen cterm=bold,italic guifg=DarkGreen gui=bold,italic
hi markdownHeadingRule ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=bold
hi markdownH1 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=bold
hi markdownH2 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=bold
hi markdownH3 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=none
hi markdownH4 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=none
hi markdownH5 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=none
hi markdownH6 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=none
hi markdownBlockquote ctermfg=DarkRed guifg=#eb4b3f
hi markdownCodeBlock ctermfg=DarkGreen guifg=#00990f
hi markdownCode ctermfg=DarkGreen guifg=#00990f
hi markdownLink ctermbg=Blue cterm=bold guibg=#603da8 gui=bold
hi markdownUrl ctermfg=DarkBlue guifg=#603da8
hi markdownLinkText ctermfg=DarkRed guifg=#eb4b3f
hi markdownLinkTextDelimiter ctermfg=DarkMagenta guifg=#e02da8
hi markdownLinkDelimiter ctermfg=DarkMagenta guifg=#e02da8
hi markdownCodeDelimiter ctermfg=DarkBlue guifg=#603da8
hi markdownId ctermfg=Magenta guifg=#ff6ca1
hi markdownIdDeclaration ctermfg=Magenta guifg=#ff6ca1
hi markdownOrderedListMarker ctermfg=Magenta guifg=#ff6ca1
hi markdownListMarker ctermfg=Magenta guifg=#ff6ca1

hi mkdBold ctermfg=DarkGreen cterm=bold guifg=#00990f gui=bold
hi mkdItalic ctermfg=DarkCyan cterm=italic guifg=#009cd9 gui=italic
hi mkdBoldItalic ctermfg=DarkGreen cterm=bold,italic guifg=DarkGreen gui=bold,italic
hi mkdListItem ctermfg=LightMagenta guifg=#ff6ca1
hi mkdInlineURL ctermbg=Blue cterm=bold guibg=#603da8 gui=bold
hi mkdHeading ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=bold
hi mkdCode ctermfg=Green guifg=#00990f
hi mkdLink ctermfg=Blue cterm=bold guifg=#603da8 gui=bold
hi mkdURL ctermfg=Blue guifg=#603da8
hi mkdString ctermfg=LightMagenta guifg=#ff6ca1
hi mkdBlockQuote ctermfg=LightCyan guifg=#009cd9
hi mkdLinkTitle ctermfg=DarkRed guifg=#eb4b3f
hi mkdDelimiter ctermfg=DarkCyan guifg=#009cd9
hi mkdRule ctermfg=DarkRed guifg=#eb4b3f
hi htmlBold ctermfg=DarkGreen cterm=bold guifg=#00990f gui=bold
hi htmlBoldItalic ctermfg=DarkGreen cterm=bold,italic guifg=#00990f gui=bold,italic
hi htmlItalic ctermfg=DarkCyan cterm=italic guifg=#009cd9 gui=italic
hi htmlH1 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=bold
hi htmlH2 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=bold
hi htmlH3 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=none
hi htmlH4 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=none
hi htmlH5 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=none
hi htmlH6 ctermfg=DarkRed cterm=bold guifg=#eb4b3f gui=none


" Terminal Theme
let g:terminal_color_0="#000000"
let g:terminal_color_1="#eb4b3f"
let g:terminal_color_2="#00990f"
let g:terminal_color_3="#dc6122"
let g:terminal_color_4="#603da8"
let g:terminal_color_5="#e02da8"
let g:terminal_color_6="#009cd9"
let g:terminal_color_7="#a2b0b0"
let g:terminal_color_8="#688088"
let g:terminal_color_9="#eb4b3f"
let g:terminal_color_10="#00990f"
let g:terminal_color_11="#dc6122"
let g:terminal_color_12="#7c86ce"
let g:terminal_color_13="#ff6ca1"
let g:terminal_color_14="#009cd9"
let g:terminal_color_15="#a2b0b0"

" vim: sw=2
